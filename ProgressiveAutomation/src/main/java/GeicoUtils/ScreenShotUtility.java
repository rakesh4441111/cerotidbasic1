package GeicoUtils;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

// Take a screenshot from the UI and store in System Explorer
public class ScreenShotUtility {
	
	public static void takeSnapShot(WebDriver driver, String screenShotName) {
		
		try {
			//Creating a File object to take screenshot
			File src = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			//Store new File
			FileUtils.copyFile(src, new File(".\\screenShots\\" + screenShotName + ".Jpeg"));
			Thread.sleep(2000);
			System.out.println("Screenshot Taken");
			
		} catch (Exception e) {
			System.out.println("Exception while taking screenshot" + e.getMessage());
			e.printStackTrace();
		}
		
	}

}
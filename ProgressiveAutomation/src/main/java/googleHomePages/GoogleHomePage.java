package googleHomePages;


import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GoogleHomePage {
	
	// Create WebDriver Obj
	WebDriver driver;
	
	public GoogleHomePage(WebDriver driver) {
		this.driver=driver;
		
		//Initialize the webelements
		PageFactory.initElements(driver,this);
	}
	
	@FindBy(xpath = "//input[@name='q']")
	WebElement searchField;
	
	@FindBy(xpath = "//div[@class='tfB0Bf']//input[@name='btnK']")
	WebElement searchBtn;
	
	public void performSearch(String search) throws InterruptedException {
		searchField.sendKeys(search);
		
		Thread.sleep(4000);
		
		searchBtn.sendKeys(Keys.RETURN);
	}
}

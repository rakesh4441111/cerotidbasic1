package progrossivePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class AboutYourVehiclePage {

	// create an instance/object of WebDriver
		private WebDriver driver;

		// constructor to create an object of this class
		public AboutYourVehiclePage(WebDriver driver) {
			this.driver = driver;

			// initialize the webelements from this class
			PageFactory.initElements(driver, this);
		}

		// locate all elements that we are going to use from page
		// annotation

		// select vehicle year field
		@FindBy(xpath = "//li[contains(text(),'2019')]")
		WebElement selectVehicleYear;
		
		// select vehicle make field
		@FindBy(xpath ="//li[contains(text(),'BMW')]")
		WebElement selectVehicleMake;
		
		// select vehicle model field
		@FindBy(xpath = "//li[contains(text(),'340')]")
		WebElement selectVehicleModel;
		
		// select primary use field
		@FindBy(xpath="//select[@id='VehiclesNew_embedded_questions_list_VehicleUse']")
		WebElement selectPrimaryUse;
		
		// miles driven miles field
		@FindBy(xpath="//input[@id='VehiclesNew_embedded_questions_list_OneWayMiles']")
		WebElement enterDrivenMiles;
		
		// select own/lease field
		@FindBy(xpath="//select[@id='VehiclesNew_embedded_questions_list_OwnOrLease']")
		WebElement selectOwnOrLease;
		
		// click Done Button
		@FindBy(xpath="//button[contains(text(),'Done')]")
		WebElement doneBtn;
		
		// click Continue Button
		@FindBy(xpath="//button[contains(text(),'Continue')]")
		WebElement continueBtn;
		
		public void enterVehicleYear() {
			selectVehicleYear.click();
		}
		
		public void enterVehicleMake() {
			selectVehicleMake.click();
		}
		
		public void enterVehicleModel() {
			selectVehicleModel.click();
		}
		
		public void selectPrimaryUse(String primaryUse) {
			WebElement element = selectPrimaryUse;
			Select choosePrimaryUse = new Select(element);
			choosePrimaryUse.selectByVisibleText(primaryUse);
			
		}
		
		public void enterDrivenMiles(String miles) {
			enterDrivenMiles.sendKeys(miles);
		}
		
		public void selectOwnOrLease(String leaseOrOwn) {
			WebElement element = selectOwnOrLease;
			Select chooseOwnOrLease = new Select(element);
			chooseOwnOrLease.selectByVisibleText(leaseOrOwn);
		}
		
		public void clickDoneBtn() {
			doneBtn.click();
		}
		
		public void clickContinueBtn() {
			continueBtn.click();
		}
}

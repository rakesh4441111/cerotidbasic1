package progrossivePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class DetailsPage {
	
	// create an instance/object of WebDriver
		private WebDriver driver;

		// constructor to create an object of this class
		public DetailsPage(WebDriver driver) {
			this.driver = driver;

			// initialize the webelements from this class
			PageFactory.initElements(driver, this);
		}

		// locate all elements that we are going to use from page
		// annotation

		// no progressive insurance history
		@FindBy(xpath = "//progressive-insurance-history[@class='ng-star-inserted']//div[3]")
		WebElement noProgressiveInsuranceHistory;
		
		// yes insurance for 3 years without break
		@FindBy(xpath="//label[@id='FinalDetailsEdit_embedded_questions_list_ContinuousInsuranceThreeYears_Label']//span[@class='control-text'][contains(text(),'Yes')]")
		WebElement yesInsuranceForThreeYears;

		// most recent bodily injury limt
		@FindBy(xpath="//select[@id='FinalDetailsEdit_embedded_questions_list_BodilyInjuryLimits']")
		WebElement recentBodilyInjuryAmount;
		
		// primary email address
		@FindBy(xpath="//input[@id='FinalDetailsEdit_embedded_questions_list_PrimaryEmailAddress']")
		WebElement emailAddress;
		
		// click continue button
		@FindBy(xpath="//button[contains(text(),'Continue')]")
		WebElement continueBtn;
		
		// click No Thank You Button
		@FindBy(xpath="//button[contains(text(),'No thanks, just auto')]")
		WebElement noThankYouBtn;
		public void noProgressiveInsuranceHistory( ) {
			noProgressiveInsuranceHistory.click();
		}
		
		public void yesInsuranceWithoutBreak() {
			yesInsuranceForThreeYears.click();
		}
		
		public void selectRecentBodilyInjuryAmout(String injuryAmount) {
			WebElement element = recentBodilyInjuryAmount;
			Select chooseRecentBodilyInjuryAmount = new Select(element);
			chooseRecentBodilyInjuryAmount.selectByVisibleText(injuryAmount);
		}
		
		public void enterPrimaryEmailAddress(String emailAdd) {
			emailAddress.sendKeys(emailAdd);
		}
		
		public void clickContinue() {
			continueBtn.click();
		}
		
		public void clickNoThankYouBtn() {
			noThankYouBtn.click();
		}
		
		
}

package progrossivePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class DriverPage {
	// create an instance/object of WebDriver
	private WebDriver driver;

	// constructor to create an object of this class
	public DriverPage(WebDriver driver) {
		this.driver = driver;

		// initialize the webelements from this class
		PageFactory.initElements(driver, this);
	}

	// locate all elements that we are going to use from page
	// annotation

	// gender
	@FindBy(xpath = "//input[@id='DriversAddPniDetails_embedded_questions_list_Gender_M']")
	WebElement gender;

	// marital status
	@FindBy(xpath = "//select[@id='DriversAddPniDetails_embedded_questions_list_MaritalStatus']")
	WebElement maritalStatus;

	// highest level of education
	@FindBy(xpath = "//select[@id='DriversAddPniDetails_embedded_questions_list_HighestLevelOfEducation']")
	WebElement highestEducation;

	// employment status
	@FindBy(xpath = "//select[@id='DriversAddPniDetails_embedded_questions_list_EmploymentStatus']")
	WebElement employmentStatus;

	// social security number
	@FindBy(xpath = "//input[@id='DriversAddPniDetails_embedded_questions_list_SocialSecurityNumber']")
	WebElement ssn;

	// primary residence
	@FindBy(xpath = "//select[@id='DriversAddPniDetails_embedded_questions_list_PrimaryResidence']")
	WebElement primaryResidence;

	// U.S. license status
	@FindBy(xpath = "//select[@id='DriversEditPniDetails_embedded_questions_list_LicenseStatus']")
	WebElement usLicenseStatus;

	// age first licensed
	@FindBy(xpath = "//input[@id='DriversAddPniDetails_embedded_questions_list_DriverAgeLicensed']")
	WebElement ageFirstLicensed;

	// first question ever license expired/suspended
	@FindBy(xpath = "//input[@id='DriversAddPniDetails_embedded_questions_list_DriverLicenseSuspended_N']")
	WebElement firstNolicenseExpiredSuspended;

	// ever licensed outside U.S.
	@FindBy(xpath = "//label[@id='DriversAddPniDetails_embedded_questions_list_DriverLicenseNonUS_Label']//span[@class='control-text'][contains(text(),'No')]")
	WebElement notEverLicensedOutsideUS;

	// license expired/suspended
	@FindBy(xpath = "//label[@id='DriversAddPniDetails_embedded_questions_list_DriverLicenseSuspended_Label']//span[@class='control-text'][contains(text(),'No')]")
	WebElement noLicenseExpiredSuspended;

	// accident claims
	@FindBy(xpath = "//input[@id='DriversAddPniDetails_embedded_questions_list_HasAccidentsOrClaims_N']")
	WebElement noAccidentClaims;

	// DUI
	@FindBy(xpath = "//input[@id='DriversAddPniDetails_embedded_questions_list_HasDwiViolations_N']")
	WebElement noDUI;

	// ticketOrviolations
	@FindBy(xpath = "//input[@id='DriversAddPniDetails_embedded_questions_list_HasTicketsOrViolations_N']")
	WebElement ticketOrViolations;

	// continue button
	@FindBy(xpath = "//button[contains(text(),'Continue')]")
	WebElement continueBtn;

	// second continue button
	@FindBy(xpath = "//button[contains(text(),'Continue')]")
	WebElement secondContinueBtn;

	// methods that utilize webelements from this page
	public void clickMaleGender() {
		gender.click();

	}

	public void selectMaritalStatus(String maritalStat) {

		WebElement element = maritalStatus;
		Select chooseMaritalStatus = new Select(element);
		chooseMaritalStatus.selectByVisibleText(maritalStat);
	}

	public void selectHighestEducation(String highEdu) {

		WebElement element = highestEducation;
		Select chooseHighestEducation = new Select(element);
		chooseHighestEducation.selectByVisibleText(highEdu);
	}

	public void selectEmploymentStatus(String employmentStat) {

		WebElement element = employmentStatus;
		Select chooseEmploymentStatus = new Select(element);
		chooseEmploymentStatus.selectByVisibleText(employmentStat);
	}

	public void enterSSN(String socialSecurityNumber) {
		ssn.sendKeys(socialSecurityNumber);

	}

	public void selectPrimaryResidence(String priResidence) {

		WebElement element = primaryResidence;
		Select choosePrimaryResidence = new Select(element);
		choosePrimaryResidence.selectByVisibleText(priResidence);
	}

	public void enterAgeFirstLicensed(String age) {
		ageFirstLicensed.sendKeys(age);

	}

	public void selectFirstNoLicenseExpired() {
		firstNolicenseExpiredSuspended.click();

	}

	public void selectNoEverLicensedOutsideUs() {
		notEverLicensedOutsideUS.click();

	}

	public void selectNoLicenseExpired() {
		noLicenseExpiredSuspended.click();

	}

	public void selectNoAccidentClaims() {
		noAccidentClaims.click();
	}

	public void selectNoDUI() {
		noDUI.click();

	}

	public void selectNoTicketOrViolations() {
		ticketOrViolations.click();
	}

	public void clickContinueBtn() {
		continueBtn.click();

	}

	public void clickSecondContinueBtn() {
		secondContinueBtn.click();
	}
}

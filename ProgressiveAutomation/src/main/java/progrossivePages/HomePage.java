package progrossivePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

	// create an instance/object of WebDriver
	private WebDriver driver;

	// constructor to create an object of this class
	public HomePage(WebDriver driver) {
		this.driver = driver;

		// initialize the webelements from this class
		PageFactory.initElements(driver, this);

	}
	
	// locate all elements that we are going to use from page
	// annotation
	
	@FindBy(xpath="//div[contains(@class,'products-bundle')]//li[1]//a[1]//p[1]")
	WebElement auto;
	
	// methods that utilize webelements from this page
	public void autoQuote() {
		auto.click();
	}
	
	public boolean isPageOpened() {
		// assert true/false if page is opened (return turn if the title contains)
		return driver.getTitle().contains("Quote Auto Insurance, Home-Auto Bundles, & More | Progressive");
	}

}

package progrossivePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class NameAndAddressPage {

	// create an instance/object of WebDriver
	private WebDriver driver;

	// constructor to create an object of this class
	public NameAndAddressPage(WebDriver driver) {
		this.driver = driver;

		// initialize the webelements from this class
		PageFactory.initElements(driver, this);
	}

	// locate all elements that we are going to use from page
	// annotation

	// first name
	@FindBy(xpath = "//input[@id='NameAndAddressEdit_embedded_questions_list_FirstName']")
	WebElement firstNameField;

	// last name
	@FindBy(xpath = "//input[@id='NameAndAddressEdit_embedded_questions_list_LastName']")
	WebElement lastNameField;

	// suffix
	@FindBy(xpath = "//select[@id='NameAndAddressEdit_embedded_questions_list_Suffix']")
	WebElement suffixField;

	// date of birth
	@FindBy(xpath = "//input[@id='NameAndAddressEdit_embedded_questions_list_DateOfBirth']")
	WebElement dateOfBirthField;
	
	// street number and name
	@FindBy(xpath = "//input[@id='NameAndAddressEdit_embedded_questions_list_MailingAddress']")
	WebElement streetNumberAndNameField;
	
	// apt number or unit number
	@FindBy(xpath = "//input[@id='NameAndAddressEdit_embedded_questions_list_ApartmentUnit']")
	WebElement aptNumberOrUnitNumberField;
	
	// city
	@FindBy(xpath = "//input[@id='NameAndAddressEdit_embedded_questions_list_City']")
	WebElement cityField;
	
	// start my quote
	@FindBy(xpath="//button[contains(text(),'Okay, start my quote.')]")
	WebElement btnOkayStartMyQuote;

	
	
	
	// methods that utilize webelements from this page

	public void enterFirstName(String firstName) {
		firstNameField.sendKeys(firstName);
	}

	public void enterLastName(String lastName) {
		lastNameField.sendKeys(lastName);
	}

	public void selectSuffix(String suffix) {
		// Creating new webelement obj and passing By(select suffix) as an argument
		WebElement element = suffixField;
		Select chooseSuffix = new Select(element);
		chooseSuffix.selectByVisibleText(suffix);
	}

	public void enterDateOfBirth(String dateOfBirth) {
		dateOfBirthField.sendKeys(dateOfBirth);

	}
	
	public void enterStreetNumberAndName(String streetNumberAndName) {
		streetNumberAndNameField.click();
		streetNumberAndNameField.clear();
		streetNumberAndNameField.sendKeys(streetNumberAndName);
		
	}
	
	public void enterAptNumberOrUnitNumber(String aptNumberOrUnitNumber) {
		aptNumberOrUnitNumberField.sendKeys(aptNumberOrUnitNumber);

	}
	
	public void enterCity(String city) {
		cityField.sendKeys(city);

	}
	
	public void clickStartMyQuote() {
		btnOkayStartMyQuote.click();
		
	}
	
	
	
}

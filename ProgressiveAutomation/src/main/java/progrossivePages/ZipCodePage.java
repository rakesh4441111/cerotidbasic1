package progrossivePages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ZipCodePage {
	
	// create an instance/object of WebDriver
		private WebDriver driver;

		// constructor to create an object of this class
		public ZipCodePage(WebDriver driver) {
			this.driver = driver;

			// initialize the webelements from this class
			PageFactory.initElements(driver, this);

		}
		
		// locate all elements that we are going to use from page
		// annotation
		
		@FindBy(xpath="//input[@id='zipCode_overlay']")
		WebElement zipCode;
		
		@FindBy(xpath="//input[@id='qsButton_overlay']")
		WebElement getAQuote;
		
		// methods that utilize webelements from this page
		public void enterZip(String zip) {
			zipCode.sendKeys(zip);
		}
		
		public void clickGetAQuote() {
			getAQuote.click();
		}
		
		public boolean isPageOpened() {
			// assert true/false if page is opened (return turn if the title contains)
			return driver.getTitle().contains("Quote Auto Insurance, Home-Auto Bundles, & More | Progressive");
		}

}

package googleHomePageTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import googleHomePages.GoogleHomePage;



public class GoogleHomePageTest {
	
	WebDriver fireDriver;
	
	@BeforeTest
	public void BeforeTest() {
		// Set system path for firefox
		System.setProperty("webdriver.gecko.driver",".\\libs\\geckodriver.exe");
	
		// create a new firefox obj
		fireDriver = new FirefoxDriver();
		fireDriver.get("https://www.google.com");
		fireDriver.manage().timeouts().implicitlyWait(7, TimeUnit.SECONDS);
			
	}
	

	
	@Test(priority =0)
	public void performSearch() throws InterruptedException {
		GoogleHomePage homePage = new GoogleHomePage(fireDriver);
		homePage.performSearch("TestNG");
		
	}
	

}

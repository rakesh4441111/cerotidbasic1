package progressivePagesTests;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;


public class PropertiesFile {
	//1:Create a object of class properties
		static Properties prop = new Properties();
		
		//2: Get system project path
		static String projectPath = System.getProperty("user.dir");
		
		public static void main(String[] args) {
			
			//1: Get current properties
			getProperties();
			
			//2: Set custom properties
			setProperties();
		}

		public static void getProperties() {
			try {
				//1: Create an object of inputstream to retrieve value from config.properties
				InputStream input = new FileInputStream(projectPath + "/src/test/java/progressivePagesTests/config.properties");
				
				//2: Load Properties File
				prop.load(input);
				
				//3: Get values from the properties file
				String browser = prop.getProperty("browser");
				System.out.println(browser +"Is invoked");
				//TODO Set browser for test case here
				StartAutoQuoteTestNG.browserName = browser;
				
				
			}catch (Exception e) {
				System.out.println(e.getMessage());
				System.out.println(e.getCause());
				e.printStackTrace();
				
			}
			
		}
		public static void setProperties() {
			try {
				//1: set data to properties file
				projectPath = System.getProperty("user.dir");
				
				//2: create a object of Output Stream
				OutputStream output = new FileOutputStream(projectPath +"/src/test/java/config/config.properties");
				
				//3: Set Values
				prop.setProperty("Result", "Pass");
				
				// Storing values in properties file
				prop.store(output, null);
				
			
				
			}catch (Exception e) {
			
		}



		}

}

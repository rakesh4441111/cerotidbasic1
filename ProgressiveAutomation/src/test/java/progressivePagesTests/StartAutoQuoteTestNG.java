package progressivePagesTests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import GeicoUtils.ScreenShotUtility;
import progrossivePages.AboutYourVehiclePage;
import progrossivePages.DetailsPage;
import progrossivePages.DriverPage;
import progrossivePages.HomePage;
import progrossivePages.NameAndAddressPage;
import progrossivePages.ZipCodePage;

public class StartAutoQuoteTestNG {

	// global variable
	WebDriver driver;

	// store value of browser name
	public static String browserName = null;

	@BeforeTest
	public void setUpTeset() {

		// Reads configuration file and invokes driver(browser) based on the
		// configuration
		PropertiesFile.getProperties();
		if (browserName.equalsIgnoreCase("chrome")) {
			// Set system path pointing to chrome driver
			System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
			driver = new ChromeDriver();

		} else if (browserName.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.chrome.driver", ".\\libs\\geckodriver.exe");
			driver = new FirefoxDriver();
		}
	}

	@Test(priority = 0)
	public void StartAutoQuote() throws InterruptedException {

		// Creating ExtentReport object and creating new extentreport html file
		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("extentReports.html");

		// Create ExtentReports and attach to the reporter
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(htmlReporter);

		// Creates a toggle for the given test, and adds logs events
		ExtentTest test = extent.createTest("Progressive Auto Quote Testing",
				"This test will check and see if we can input data and get the quote in Progressive website");

		// Logger example (log message info to UI)
		test.log(Status.INFO, "Executing Progressive Website");

		// create an object of the HomePage to utilize UI Elements
		HomePage homePage = new HomePage(driver);
		driver.get("https://www.progressive.com");
		Thread.sleep(6000);

		if (driver.getTitle().contains("Quote Auto Insurance, Home-Auto Bundles, & More | Progressive")) {
			// Logger example (log message for pass status)
			test.pass("Navigated to Progressive Website");
		}

		// Screenshot of homepage
		ScreenShotUtility.takeSnapShot(driver, "Progressive Home Page");
		Thread.sleep(3000);
		// validate if the HomePage is opened (Junit Framework)
		Assert.assertTrue(homePage.isPageOpened());

		// click start Auto Quote
		homePage.autoQuote();

		// enter zip code on ZipCodePage
		ZipCodePage zip = new ZipCodePage(driver);
		zip.enterZip("94014");
		// Screenshot of zipcode
		ScreenShotUtility.takeSnapShot(driver, "Zip Code");

		// click on Get a Quote
		zip.clickGetAQuote();

		// enter data in the NameAndAddressPage

		Thread.sleep(4000);

		NameAndAddressPage nameAndAddressPage = new NameAndAddressPage(driver);

		// enter firstname in the NameAndAddressPage
		nameAndAddressPage.enterFirstName("Rakesh");

		// enter lastname in the NameAndAddressPage
		nameAndAddressPage.enterLastName("Shrestha");

		// choose suffix in the NameAndAddressPage
		nameAndAddressPage.selectSuffix("Sr");

		// enter dateofbirth in the NameAndAddressPage
		nameAndAddressPage.enterDateOfBirth("10/10/1988");

		// enter street number and name in the NameAndAddressPage
		nameAndAddressPage.enterStreetNumberAndName("3 puma street");
		nameAndAddressPage.enterStreetNumberAndName("3 puma street");

		// enter apt number or unit number in the NameAndAddressPage
		nameAndAddressPage.enterAptNumberOrUnitNumber("3");

		// enter city in the NameAndAddressPage
		nameAndAddressPage.enterCity("Richmond");

		// Screenshot of Name and Address
		ScreenShotUtility.takeSnapShot(driver, "Name and Address Page");
		Thread.sleep(3000);

		// click button Okay Stary My Quote
		nameAndAddressPage.clickStartMyQuote();

		Thread.sleep(7000);
		// select data in the AboutYourVehiclePage
		AboutYourVehiclePage aboutYourVehicle = new AboutYourVehiclePage(driver);

		// select vehicle year
		aboutYourVehicle.enterVehicleYear();

		Thread.sleep(5000);

		// select vehicle make
		aboutYourVehicle.enterVehicleMake();

		Thread.sleep(1000);
		// select vehicle model
		aboutYourVehicle.enterVehicleModel();

		Thread.sleep(1000);
		// select primary use
		aboutYourVehicle.selectPrimaryUse("Personal (to/from work, school, errands)");

		Thread.sleep(1000);
		// enter driven miles
		aboutYourVehicle.enterDrivenMiles("40");

		Thread.sleep(1000);
		// select own or lease
		aboutYourVehicle.selectOwnOrLease("Own");

		// Screenshot of Vehicle Page
		ScreenShotUtility.takeSnapShot(driver, "Vehicle Page");
		Thread.sleep(3000);

		// click Done Button
		aboutYourVehicle.clickDoneBtn();

		Thread.sleep(4000);
		// click Continue Button
		aboutYourVehicle.clickContinueBtn();

		// enter data in the driverPage
		Thread.sleep(4000);
		DriverPage driverPage = new DriverPage(driver);

		Thread.sleep(3000);
		// click male gender
		driverPage.clickMaleGender();
		Thread.sleep(2000);
		// select married on married status
		driverPage.selectMaritalStatus("Single");
		Thread.sleep(2000);
		// select highest level of education
		driverPage.selectHighestEducation("College degree");
		Thread.sleep(3000);
		// select employment status
		driverPage.selectEmploymentStatus("Student (full time)");
		Thread.sleep(3000);
		// enter ssn
		driverPage.enterSSN("458122323");
		Thread.sleep(3000);
		// enter primary residence
		driverPage.selectPrimaryResidence("Rent");

		Thread.sleep(2000);
		// enter age first licensed
		driverPage.enterAgeFirstLicensed("22");

		Thread.sleep(3000);
		// click no license expired rovoked
		driverPage.selectFirstNoLicenseExpired();
		Thread.sleep(5000);

		// click no of ever licensed outside US
		driverPage.selectNoEverLicensedOutsideUs();
		Thread.sleep(4000);

		// click no license expired rovoked
		driverPage.selectNoLicenseExpired();
		Thread.sleep(4000);

		// click no accidents claims damages
		driverPage.selectNoAccidentClaims();
		Thread.sleep(2000);
		// click no dui
		driverPage.selectNoDUI();
		Thread.sleep(2000);

		// click no ticket/violations
		driverPage.selectNoTicketOrViolations();
		Thread.sleep(2000);

		// Screenshot of driver page
		ScreenShotUtility.takeSnapShot(driver, "Driver Page");
		Thread.sleep(3000);

		// click continue button
		driverPage.clickContinueBtn();
		Thread.sleep(5000);

		// click second continue button in driver page
		driverPage.clickSecondContinueBtn();

		Thread.sleep(5000);
		// enter data into DetailsPage
		DetailsPage detailsPage = new DetailsPage(driver);

		// select no progressive insurance history
		detailsPage.noProgressiveInsuranceHistory();

		// select yes auto insurance history
		detailsPage.yesInsuranceWithoutBreak();

		// select most recent bodily insurance limit
		detailsPage.selectRecentBodilyInjuryAmout("$25,000/$50,000");
		Thread.sleep(3000);
		// enter primary email address
		detailsPage.enterPrimaryEmailAddress("abc@gmail.com");

		// Screenshot of Details page
		ScreenShotUtility.takeSnapShot(driver, "Details Page");
		Thread.sleep(3000);

		// click continue button
		detailsPage.clickContinue();
		Thread.sleep(3000);
		// click no thank you button
		detailsPage.clickNoThankYouBtn();
		Thread.sleep(3000);

		test.log(Status.INFO, "Ending Progressive Auto Quote Page Test");
		test.pass("Closed Browser");
		extent.flush();

		// Screenshot of final quote page
		ScreenShotUtility.takeSnapShot(driver, "Final Quote Page");
		Thread.sleep(3000);
	}
	
	@AfterTest
	public void AfterTest() {
		driver.close();
		driver.quit();
	}

}

package DesiredCapabilities;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class DesiredCapabilitiesDemo {
	// 1: How to invoke browser in incognito mode
	// 2: HOw to invoke browser in headles mode
	// 3: How to remove adds in a browser

	public static void main(String[] args) {
		// set the system path
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

//	//Invoke Browser in incognito mode
//	ChromeOptions options = new ChromeOptions();
//	options.addArguments("--incognito");
//	
//	WebDriver driver = new ChromeDriver(options);
//	driver.get("https://www.google.com");
//	

//	// Invoke Browser in Headless mode
//	ChromeOptions options = new ChromeOptions();
//	options.addArguments("--headless");
//	WebDriver driver = new ChromeDriver(options);
//	
//	driver.get("https://www.google.com");
//	System.out.println(driver.getPageSource());
//	System.out.println(driver.getTitle());

		// Ad Blocker 
		ChromeOptions options = new ChromeOptions();
		options.addExtensions(new File(".\\libs\\extension_20_6_17_2.crx"));
		WebDriver driver = new ChromeDriver(options); //
		driver.get("https://www.guru99.com/smoke-testing.html#7");

	}
}

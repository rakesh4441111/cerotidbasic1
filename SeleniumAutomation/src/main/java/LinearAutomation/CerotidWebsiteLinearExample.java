package LinearAutomation;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CerotidWebsiteLinearExample {

	public static void main(String[] args) {
		// Step 1: Invoke Browser: Navigate to the Cerotid Page
		//1. Set the System Path
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		
		//b. Create a ChromeDriver Object
		WebDriver driver = new ChromeDriver();
		
		//c. Navigate to url
		driver.navigate().to("http://www.cerotid.com");
		
	
		// Step 2: Fill form in Cerotid Page
		//a. Created a WebElement object
		WebElement selectCourse = driver.findElement(By.xpath("//select[@id='classType']"));
		
		//b. Create a select obj and pass the element we want to select
		Select chooseCourse = new Select(selectCourse);
		
		//c. creating a string variable with course name
		String courseName = "QA Automation";
		//d. Selecting the course by visible text
		chooseCourse.selectByVisibleText(courseName);
		
		// Create a Session type Webelement object
		WebElement sessionType = driver.findElement(By.xpath("//select[@id='sessionType']"));
		Select chooseSession = new Select(sessionType);
		String session = "Upcoming Session";
		chooseSession.selectByVisibleText(session);
		
		// Create a Address type Webelement object
		WebElement addressField = driver.findElement(By.xpath("//input[@placeholder='Address *']"));
		addressField.sendKeys("14 3rd ave, Daly City");
		
		// Creating city WebElement
		WebElement cityField = driver.findElement(By.xpath("//input[id='city']"));
		cityField.sendKeys("14 3rd ave Daly City");
		
		
		
		
		//Create First Name Webelement
		WebElement firstName = driver.findElement(By.xpath("//input[@placeholder='Full Name *']"));
		firstName.sendKeys("Test");
		
		// Clicking on the No Radio Button
		//Create WEbElement Object
		WebElement ableToRelocateNOBtn = driver.findElement(By.xpath("//input[@value='N0']"));
		ableToRelocateNOBtn.click();
		
		
		// Step 3: Validate Success Message

	}

}



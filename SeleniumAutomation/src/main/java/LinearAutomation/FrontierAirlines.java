package LinearAutomation;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class FrontierAirlines {

	public static void main(String[] args) throws InterruptedException {
		
System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		
		//b. Create a ChromeDriver Object
		WebDriver driver = new ChromeDriver();
		
		//c. Navigate to url
		driver.navigate().to("https://www.flyfrontier.com");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);
		
		//Close cookies
		driver.findElement(By.xpath("//button[@class='onetrust-close-btn-handler onetrust-close-btn-ui banner-close-button onetrust-lg close-icon']")).click();
		Thread.sleep(5000);
		
		
		//Select Departure From
		driver.findElement(By.xpath("//*[@id='kendoDepartFrom_input']")).sendKeys("DFW");	
		Thread.sleep(5000);
		
		// Create object of Action class
		Actions action = new Actions(driver);
 
		// Sendkeys using Action class object
		action.sendKeys(Keys.ENTER).build().perform();
	
	
		Thread.sleep(5000);
		
		//Select Arrival To
		
		driver.findElement(By.xpath("//input[@id='kendoArrivalTo_input']")).sendKeys("LAX");
		
		
		
		
		// Create object of Action class
		Actions action1 = new Actions(driver);
		 
		// Sendkeys using Action class object
		action1.sendKeys(Keys.ENTER).build().perform();
	
		
		
		// Selecting departure
		
		driver.findElement(By.xpath("//img[@id='departureDateIcon']")).click();
		driver.findElement(By.xpath("//a[@id='7-15-2020']")).click();
		
		
		Thread.sleep(5000);
		
		
		// Selecting arrival
		driver.findElement(By.xpath("//img[@id='returnDateIcon']")).click();
		driver.findElement(By.xpath("//a[@id='7-22-2020']")).click();
		
		
		//Adding Passenger
		driver.findElement(By.xpath("//div[@class='display-inline']//div[@class='arrow-container']")).click();
		driver.findElement(By.xpath("//img[@class='add-adult']")).click();
		driver.findElement(By.xpath("//div[@class='invisible-click-handler-input']")).click();
		
		
	
		
		// Click search button
		driver.findElement(By.xpath("//a[@id='btnSearch']//img[@class='SearchSVG']")).click();
		Thread.sleep(5000);
		
		
		
	}

}
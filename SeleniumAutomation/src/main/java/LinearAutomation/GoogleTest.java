package LinearAutomation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class GoogleTest {

	public static void main(String[] args) {
		//Creating wedriver obj
		//Setting the System path - providing location of the chromedriver
		
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.google.com");
		driver.manage().window().maximize();

		//validate that the title is "google"
		if(driver.getTitle().contains("Google")) {
			System.out.println("Passed");
		}else {
			System.out.println("Failed:Invalid page");
			
		}
		
		//Perform some action on UI/webpage
		
		
		
		
		//Close the Browser
		driver.close();
	}

}

package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

//Rakesh : This class will hold web elements from cerotid and help us understand page object model
//This class will return elements to class CerotidPageTest

public class CerotidPage {

	// Class level variable to store web element value
	private static WebElement element = null;

	// Course element
	public static WebElement selectCourse(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//select[@id='classType']"));
		return element;

	}

	// Session element
	public static WebElement selectSession(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//select[@id='sessionType']"));
		return element;

	}

	// Full Name element
	public static WebElement fullName(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//input[@placeholder='Full Name *']"));
		return element;

	}

	// Address element
	public static WebElement addressField(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//input[@placeholder='Address *']"));
		return element;

	}

	// City element
	public static WebElement cityField(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//input[@placeholder='City *']"));
		return element;

	}

	// State element
	public static WebElement selectState(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//select[@id='state']"));
		return element;

	}

	// Zipcode element
	public static WebElement zipField(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//input[@id='zip']"));
		return element;

	}

	// Email element
	public static WebElement emailField(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//input[@placeholder='Email *']"));
		return element;

	}

	// Phone element
	public static WebElement phoneField(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//input[@placeholder='Phone *']"));
		return element;

	}
	
	// Visa Status element
	public static WebElement selectVisaStatus(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//select[@id='visaStatus']"));
		return element;

	}
	
	// How did you hear about us element
	public static WebElement selectAboutUs(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//select[@id='mediaSource']"));
		return element;

	}
	
	// Relocate No element
	public static WebElement relocateNoBtn(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//input[@value='N0']"));
		return element;

	}
	
	// Education text details area element
	public static WebElement eduTextArea(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//textarea[@id='eduDetails']"));
		return element;

	}
	
	// Submit element
	public static WebElement submitBtn(WebDriver driver) {
		// find the element and return its value
		element = driver.findElement(By.xpath("//button[@id='registerButton']"));
		return element;

	}
	
	

}

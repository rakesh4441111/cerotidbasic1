package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class CerotidPageObjectsAndMethods {
	// Creating a empty webdriver
	WebDriver driver = null;

	// Locating Elements using By Object
	By selectCourse = By.xpath("//select[@id='classType']");
	By selectSession = By.xpath("//select[@id='sessionType']");
	By fullName = By.xpath("//input[@placeholder='Full Name *']");
	By address = By.xpath("//input[@placeholder='Address *']");
	By city = By.xpath("//input[@placeholder='City *']");
	By selectState = By.xpath("//select[@id='state']");
	By zip = By.xpath("//input[@id='zip']");
	By email = By.xpath("//input[@placeholder='Email *']");
	By phone = By.xpath("//input[@placeholder='Phone *']");
	By visaStatus = By.xpath("//select[@id='visaStatus']");
	By hearAboutUs = By.xpath("//select[@id='mediaSource']");
	By relocateNoBtn = By.xpath("//input[@value='N0']");
	By eduDetails = By.xpath("//textarea[@id='eduDetails']");

	// Creating Constructor so we can utilize the web driver
	public CerotidPageObjectsAndMethods(WebDriver driver) {
		this.driver = driver;

	}

	// Creating methods that let me interact with elements

	// Select course
	public void selectCourse(String course) {
		// Creating new webelement obj and passing By(select course) as an argument
		WebElement element = driver.findElement(selectCourse);
		Select chooseCourse = new Select(element);
		chooseCourse.selectByVisibleText(course);

	}

	// Select session date
	public void selectSessionDate(String sessionDate) {
		// Creating new webelement obj and passing By(select course) as an argument
		WebElement element = driver.findElement(selectSession);
		Select chooseSession = new Select(element);
		chooseSession.selectByVisibleText(sessionDate);

	}

	// Full name element
	public void fullName(String name) {
		// Creating new webelement obj and passing By(select course) as an argument
		driver.findElement(fullName).sendKeys(name);
		;

	}

	// Address element
	public void address(String addressValue) {
		// Creating new webelement obj and passing By(select course) as an argument
		driver.findElement(address).sendKeys(addressValue);

	}

	// City element
	public void city(String cityValue) {
		// Creating new webelement obj and passing By(select course) as an argument
		driver.findElement(city).sendKeys(cityValue);
		;

	}

	// Select state
	public void selectState(String stateValue) {
		// Creating new webelement obj and passing By(select course) as an argument
		WebElement element = driver.findElement(selectState);
		Select chooseState = new Select(element);
		chooseState.selectByVisibleText(stateValue);

	}
	
	// Zip element
	public void zip(String zipValue) {
		// Creating new webelement obj and passing By(select course) as an argument
		driver.findElement(zip).sendKeys(zipValue);

}
	// Email element
	public void email(String emailValue) {
		// Creating new webelement obj and passing By(select course) as an argument
		driver.findElement(email).sendKeys(emailValue);

}
	// Phone element
	public void phone(String phoneValue) {
		// Creating new webelement obj and passing By(select course) as an argument
		driver.findElement(phone).sendKeys(phoneValue);

}
	
	// Select visa status
	public void selectVisa(String visaStatusValue) {
		// Creating new webelement obj and passing By(select course) as an argument
		WebElement element = driver.findElement(visaStatus);
		Select chooseVisaStatus = new Select(element);
		chooseVisaStatus.selectByVisibleText(visaStatusValue);
	}
	
	// Select how did you hear about us
	public void selectAboutUs(String aboutUsValue) {
		// Creating new webelement obj and passing By(select course) as an argument
		WebElement element = driver.findElement(hearAboutUs);
		Select chooseAboutUs = new Select(element);
		chooseAboutUs.selectByVisibleText(aboutUsValue);
	}
	
	public void relocateNoBtn() {
		// Creating new webelement obj and passing By(select course) as an argument
		driver.findElement(relocateNoBtn).click();
	
}
	
	// Education Details element
	public void eduDetails(String eduDetailsValue) {
		// Creating new webelement obj and passing By(select course) as an argument
		driver.findElement(eduDetails).sendKeys(eduDetailsValue);

}
}
	

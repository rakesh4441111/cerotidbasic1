package tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import pages.CerotidPageObjectsAndMethods;

public class CerotidPageObjectsAndMethodsTest {

	public static WebDriver driver = null;

	public static void main(String[] args) {
		// 1: invoke browser
		invokeBrowser();

		// 2: Fill form
		fillForm();

		// 3: Validate Success message
		validateSuccessMessage();

		// 4: Terminate the browser
		terminateBrowser();

	}

	// Step 1
	private static void invokeBrowser() {
		// 1. Set the System Path
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

		// b. Create a ChromeDriver Object
		driver = new ChromeDriver();

		// c. Navigate to url
		driver.navigate().to("http://www.cerotid.com");

		// d. Validate the title
		if (driver.getTitle().contains("Cerotid")) {
			System.out.println(driver.getTitle() + "-------Launched");
		}

		// e. Maximize window
		driver.manage().window().maximize();

		// f.Sleep
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

	}

	// Step 2
	private static void fillForm() {
		CerotidPageObjectsAndMethods cerotidPageObj = new CerotidPageObjectsAndMethods(driver);
		cerotidPageObj.selectCourse("Java");
		cerotidPageObj.selectSessionDate("Upcoming Session");
		cerotidPageObj.fullName("Rakesh");
		cerotidPageObj.address("123 boston ave");
		cerotidPageObj.city("San Francisco");
		cerotidPageObj.selectState("CA");
		cerotidPageObj.zip("98765");
		cerotidPageObj.email("abc@hotmail.com");
		cerotidPageObj.phone("4561237895");
		cerotidPageObj.selectVisa("OPT");
		cerotidPageObj.selectAboutUs("Social Media");
		cerotidPageObj.relocateNoBtn();
		cerotidPageObj.eduDetails("Computer Science");

	}

	// Step 3
	private static void validateSuccessMessage() {
		String expectedMessage = "Your register is completed. We will contact you shortly!";

		String actualMessage = driver.findElement(By.xpath("//strong[contains(text(),'Your register is completed.')]"))
				.getText();

		if (expectedMessage.equalsIgnoreCase(actualMessage)) {
			System.out.println("Pass: Expected Message " + expectedMessage + " was displayed");
		} else {
			System.out.println("Failed: Expected Message was not displayed");

		}

	}

	// Step 4
	private static void terminateBrowser() {
		// Close will terminate tab
		driver.close();

		// Terminate the browser
		driver.quit();

	}

}

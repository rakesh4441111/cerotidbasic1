package tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import pages.CerotidPage;

public class CerotidPageTest {

	private static  WebDriver driver = null;

	public static void main(String[] args) {

		// 1: invoke browser
		invokeBrowser();

		// 2: Fill form
		fillForm();
		
		// 3: Validate success message
		validateSuccessMessage();

		// 4: Terminate browser
		terminateBrowser();
		
		

	}

	private static void terminateBrowser() {
		
		// Close will terminate tab
		driver.close();
		
		// Terminate the browser
		driver.quit();
		
	}

	private static void validateSuccessMessage() {
		String expectedMessage = "Your register is completed. We will contact you shortly!";
		
		String actualMessage = driver.findElement(By.xpath("//strong[contains(text(),'Your register is completed.')]")).getText();
		
		if(expectedMessage.equalsIgnoreCase(actualMessage)) {
			System.out.println("Pass: Expected Message " + expectedMessage + " was displayed");
		}else {
			System.out.println("Failed: Expected Message was not displayed");
	
				
			}
	}

	public static void invokeBrowser() {
		// 1. Set the System Path
		System.setProperty("webdriver.chrome.driver", ".\\libs\\chromedriver.exe");

		// b. Create a ChromeDriver Object
		 driver = new ChromeDriver();

		// c. Navigate to url
		driver.navigate().to("http://www.cerotid.com");

		// d. Validate the title
		if (driver.getTitle().contains("Cerotid")) {
			System.out.println(driver.getTitle() + "-------Launched");
		}

		// e. Maximize window
		driver.manage().window().maximize();
		
		//f.Sleep
		driver.manage().timeouts().implicitlyWait(15,TimeUnit.SECONDS);

	}

	public static void fillForm() {
		try {
			// Utilizing the CerotidPage Objects/Elements to select course
			Select chooseCourse = new Select(CerotidPage.selectCourse(driver));
			chooseCourse.selectByVisibleText("QA Automation");

			// Utilizing the CerotidPage Objects/Elements to select session
			Select chooseSession = new Select(CerotidPage.selectSession(driver));
			chooseSession.selectByVisibleText("Upcoming Session");
			
			// Utilizing the CerotidPage Objects/Elements to Enter the name in full name field
			CerotidPage.fullName(driver).sendKeys("John Martin");
			
			// Utilizing the CerotidPage Objects/Elements to Enter the name in city field
			CerotidPage.addressField(driver).sendKeys("123 4th ave, Daly City, California");
			
			// Utilizing the CerotidPage Objects/Elements to Enter the name in city field
			CerotidPage.cityField(driver).sendKeys("Daly City");
			
			// Utilizing the CerotidPage Objects/Elements to select state
			Select chooseState = new Select(CerotidPage.selectState(driver));
			chooseState.selectByVisibleText("CA");
			
			// Utilizing the CerotidPage Objects/Elements to Enter the Zip in the zip field
			CerotidPage.zipField(driver).sendKeys("98076");
			
			// Utilizing the CerotidPage Objects/Elements to Enter the email in the email field
			CerotidPage.emailField(driver).sendKeys("rakesh123@hotmail.com");
			
			// Utilizing the CerotidPage Objects/Elements to Enter the phone in the phone field
			CerotidPage.phoneField(driver).sendKeys("4152436567");
			
			// Utilizing the CerotidPage Objects/Elements to select visa status
			Select chooseVisaStatus = new Select(CerotidPage.selectVisaStatus(driver));
			chooseVisaStatus.selectByVisibleText("OPT");
			
			// Utilizing the CerotidPage Objects/Elements to select how did you hear about us
			Select chooseAboutUs = new Select(CerotidPage.selectAboutUs(driver));
			chooseAboutUs.selectByVisibleText("Social Media");
			
			// Utilizing the CerotidPage Objects/Elements to choose No Relocate button
			CerotidPage.relocateNoBtn(driver).click();
			
			// Utilizing the CerotidPage Objects/Elements to Enter the education details in text area
			CerotidPage.eduTextArea(driver).sendKeys("Computer Science");
			
			// Utilizing the CerotidPage Objects/Elements to Enter the education details in text area
			CerotidPage.submitBtn(driver).click();
			

			
		} catch (Exception e) {
			e.getStackTrace();
			e.getMessage();
		}

}
}
